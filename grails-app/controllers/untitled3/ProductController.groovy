package untitled3

class ProductController {

    def index() {
        render view: '/product/index'
    }

    def save() {
        def product = new Product(params)
        product.save()
        render "Success!"
    }
}
