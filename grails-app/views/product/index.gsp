<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Form</title>
</head>

<body>
<g:form controller="product" action="save">
    <label>Product Name:</label>
    <g:textField name="productName"/><br/>
    <label>Product Code:</label>
    <g:textField name="productCode"/><br/>
    <label>Price:</label>
    <g:textField name="price"/><br/>
    <g:actionSubmit value="Save"/>
</g:form>
</body>
</html>