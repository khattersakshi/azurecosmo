<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Form</title>
</head>

<body>
<g:form controller="placeOrder" action="save">
    <label>Detail Name:</label>
    <g:textField name="detail"/><br/>
    <label>Order Number:</label>
    <g:textField name="orderNumber"/><br/>

    <g:actionSubmit value="Save"/>
</g:form>
</body>
</html>