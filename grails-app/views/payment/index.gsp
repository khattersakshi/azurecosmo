<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Form</title>
</head>

<body>
<g:form controller="payment" action="save">
    <label>Cheque Number:</label>
    <g:textField name="chequeNumber"/><br/>
    <label>Payment Amount:</label>
    <g:textField name="paymentAmount"/><br/>
    <label>Payment Source:</label>
    <g:textField name="paymentSource"/><br/>

    <g:actionSubmit value="Save"/>
</g:form>
</body>
</html>