<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Form</title>
</head>

<body>
<g:form controller="customer" action="save">
    <label>Customer Name:</label>
    <g:textField name="customerName"/><br/>
    <label>Customer Number:</label>
    <g:textField name="customerNumber"/><br/>
    <label>Address:</label>
    <g:textField name="address"/><br/>
    <label>Gender:</label>
    <g:textField name="gender"/><br/>
    <g:actionSubmit value="Save"/>
</g:form>
</body>
</html>