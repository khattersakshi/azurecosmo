dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    dialect = 'org.hibernate.dialect.MySQL5InnoDBDialect'
}

environments {
    development {
        grails.serverURL = "localhost:8080"

        dataSource {
            dbCreate = "create-drop"
          url = "jdbc:mysql://localhost:3306/demo?autoreconnect=true"
//            url = "jdbc:mysql://prashant-mysql.mysql.database.azure.com:3306/demo?verifyServerCertificate=true&useSSL=true&requireSSL=false"
          username = "root"
//            username = "rootuser@prashant-mysql"
            logSql = false
          password = "nextdefault"
//            password = "Fin@default1"


        }
    }
    test {
        dataSource {
            dbCreate = "none"
            url = "jdbc:mysql://localhost:3306/test_demo?autoreconnect=true"
            username = "root"
            logSql = false
            password = "nextdefault"
        }
    }
    production {


        dataSource {
            username = "root"
            password = "nextdefault"
            dbCreate = "none"
            url = "jdbc:mysql://localhost:3306/p2p_invoice_prod?autoreconnect=true"
            pooled = true
            properties {
                maxActive = -1
                minEvictableIdleTimeMillis = 1800000
                timeBetweenEvictionRunsMillis = 1800000
                numTestsPerEvictionRun = 3
                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = true
                validationQuery = "SELECT 1"
            }
        }

    }
}